function [ Y ] = stft( x, Fs, framelength_ms, overlap_percent )
%stft calculates and returns the STFT of every timeframe.
%   Every row of the output matrix Y contains the frequency bins of one
%   timeframe of length "framelength_ms" milliseconds.

% Frame length in samples
framelength_samples = round(Fs * framelength_ms / 1000);
% Using Hamming window
window = hamming(framelength_samples);
% Overlap of timeframes in samples
overlap_samples = (overlap_percent/100)*framelength_samples;
% Stepsize used in the for loop
stepsize = framelength_samples - overlap_samples;
% Calculate appropiate FFT length
fftlength = 256;
while framelength_samples > fftlength
    fftlength = 2*fftlength;
end
% Number of (complete) frames
frames = ceil((length(x) - framelength_samples) / stepsize);
% Initializing matrix Y where each column represents the FFT values of one timeframe
Y = zeros(frames, fftlength);
% STFT calculation
offset = 0;
for i = 1:frames
    Y(i,:) = fft(window .* x(1 + offset:offset + framelength_samples),fftlength);
    offset = offset + stepsize;
end
% Last frame padded with zeros to have framelength_samples
if (mod(length(x) - framelength_samples,stepsize) ~= 0)
    lastframe = zeros(framelength_samples,1);
    lastframelength = length(x(1 + frames * stepsize:end));
    lastframe(1:lastframelength) = x(1 + frames * stepsize:end);
    Y(frames + 1,:) = fft((window .* lastframe),fftlength);
end
end
