function Rnn = cov_estimate(soundfile,fs,tw,ov)
%% Frequency-domain covariance matrices estimation
% This script estimates the covariance matrices of a reference noise signal
% in the frequency domain. These matrices can then be used to perform
% multichannel wiener filtering on a noisy signal.
%
% Requires:
%       sft.m -   Short-time Fourier transform 
%
% Input: 
%   - soundfile (string) : Path to an M-channel reference noise sound file
%   - fs (int) : Desired frequency of sampling
%   - tw (double) : Spectrogram time window's length in milisecond
%   - ov (double) : Percentage of overlap between consecutive time windows
%
% Output:
%   - Rnn (MxMxF) : Rnn(:,:,f) is the estimated covariance at frequency f
%
% Author: Antoine Deleforge
% Contact: deleforge@LNT.de
% Lehrstuhl für Multimediakommunikation und Signalverarbeitung
% University of Erlangen-Nuremberg, 2014

%% Extract signals from file
[nn, Fs] = audioread(soundfile); % Load signal
M = size(nn,2);
% resample signal to desired frequency fs:
n1=resample(nn(:,1), fs, Fs);
Q=size(n1,1);
n=zeros(Q,M);
n(:,1)=n1;
for m=2:M
    n(:,m) = resample(nn(:,m), fs, Fs);
end

%% Short time fourier transform
N1 = stft(n(:,1), fs, tw, ov)';
[F,T] = size(N1);
N=zeros(M,F,T);
N(1,:,:)=N1;
for m=2:M
    N(m,:,:)= stft(n(:,m), fs, tw, ov)';
end


%% Compute noise covariance matrices
Rnn = zeros(M,M,F);
for t=1:T
    for f=1:F  
            Rnn(:,:,f) = Rnn(:,:,f)+N(:,f,t)*N(:,f,t)';
    end	
end
Rnn = Rnn./T + 1e-10; % (regularization)
end