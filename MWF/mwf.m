function out = mwf(soundfile,Rnn,fs,tw,ov)
%% Multichannel Wiener filtering
% This script applies multi-channel Wiener filtering to an M-channel
% recording, using the reference noise covariance matrices in Rnn. It
% outputs a clean M-channel signal.
%
% Requires:
%       sft.m -   Short-time Fourier transform 
%       istft.m - Inverse short-time Fourier transform
%
% Input: 
%   - soundfile (string) : Path to an M-channel sound file
%   - Rnn (MxMxF) : Rnn(:,:,f) is the noise covariance at frequency f
%   - fs (int) : Desired frequency of sampling
%   - tw (double) : Spectrogram time window's length in milisecond
%   - ov (double) : Percentage of overlap between consecutive time windows
%
% Output:
%   - out (Q x M) : Clean M-channel signal
%
% Reference:
%   I. Cohen, J. Benesty, and S. Gannot, Speech Processing in Modern
%   Communication, Springer, Berlin, Heidelberg, 2010, Chapter 9.

%
% Authors: Antoine Deleforge - Stefan Meier - Alexander Valyalkin
% Contact: deleforge@LNT.de
% Lehrstuhl für Multimediakommunikation und Signalverarbeitung
% University of Erlangen-Nuremberg, 2014

%% Internal Parameters
cfg.lambda = 0.999;  % Forgetting factor
cfg.MWF_Reg = 1e-10; % Regularization
cfg.SDW = 1; % Speech Distortion Weigthed MWF parameter

%% Extract signals from file
[xx, Fs] = audioread(soundfile); % Load signal
M = size(xx,2);
% resample signal to desired frequency fs:
x1=resample(xx(:,1), fs, Fs);
Q=size(x1,1);
n=zeros(Q,M);
x(:,1)=x1;
for m=2:M
    x(:,m) = resample(xx(:,m), fs, Fs);
end

%% Short time fourier transform
X1 = stft(x(:,1), fs, tw, ov)';
[F,T] = size(X1);
X=zeros(M,F,T);
X(1,:,:)=X1;
for m=2:M
    X(m,:,:)= stft(x(:,m), fs, tw, ov)';
end

%% Multichannel Wiener Filtering
Rxx = repmat(eye(M),1,1,F);
Out=zeros(M,F,T);

for t=1:T
	for f=1:F
        % Recursive averaging:
		Rxx(:,:,f) = cfg.lambda*Rxx(:,:,f) + (1-cfg.lambda) * X(:,f,t)*X(:,f,t)';
        
		tmp_inv = Rnn(:,:,f)\Rxx(:, :, f);
		G = (tmp_inv - eye(M))./(1/cfg.SDW + trace(tmp_inv) - M);
		
		Out(:,f,t) = G' * X(:,f,t);
	end	
end

%% Obtaining time-domain signals
out1 = istft( squeeze(Out(1,:,:))', fs, tw, ov);
Q = numel(out1);
out = zeros(Q,M);
out(:,1)=out1;
for m=1:M
    out(:,m)=istft(squeeze(Out(m,:,:))', fs, tw, ov);
end

end